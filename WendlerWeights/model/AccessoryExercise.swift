import Foundation

struct AccessoryExercise: Codable {
    let id = UUID()
    let name: String
    let sets: Int
    let reps: Int
}
