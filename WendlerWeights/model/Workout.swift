import Foundation

struct Workout: Codable {
    var id = UUID()
    let exercise: Exercise
    let accessoryExercises: [AccessoryExercise]

    func copy(
        exercise: Exercise? = nil,
        accessoryExercises: [AccessoryExercise]? = nil
    ) -> Workout {
        var workout = Workout(
            exercise: exercise ?? self.exercise,
            accessoryExercises: accessoryExercises ?? self.accessoryExercises
        )
        workout.id = self.id
        
        return workout
    }
}
