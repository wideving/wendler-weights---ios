import Foundation

struct WorkoutSet {
    let number: Int
    let reps: String
    let weight: Double
    let unitOfMass: String
}
