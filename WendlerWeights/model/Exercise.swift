import Foundation

struct Exercise: Codable {
    let name: String
    let weight: Double
    let increment: Double

    func copy(
        name: String? = nil,
        weight: Double? = nil,
        increment: Double? = nil
    ) -> Exercise {
        return Exercise(
            name: name ?? self.name,
            weight: weight ?? self.weight,
            increment: increment ?? self.increment
        )
    }
}
