import Foundation

struct Cycle: Identifiable {
    let id = UUID()
    let workouts: [Workout]
    let load: Load
}
