import Foundation

enum Load: Int {
    case light = 0
    case medium
    case heavy
    case deload

    var percentages: [Double] {
        switch self {
        case .light:
            return [0.65, 0.75, 0.85]
        case .medium:
            return [0.70, 0.80, 0.90]
        case .heavy:
            return [0.85, 0.90, 0.95]
        case .deload:
            return [0.40, 0.50, 0.60]
        }
    }

    var reps: [String] {
        switch self {
        case .light:
            return ["5", "5", "5+"]
        case .medium:
            return ["3", "3", "3+"]
        case .heavy:
            return ["5", "3", "1+"]
        case .deload:
            return ["5", "5", "5"]
        }
    }
}
