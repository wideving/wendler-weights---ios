import Combine
import Foundation

class CycleViewModel: ObservableObject {
    private let repository = WorkoutRepository()
    private var anyCancellable: AnyCancellable?
    
    let weekTitles = ["Light", "Medium", "Heavy", "Deload"]
    
    @Published var week: Int = 0
    @Published var cycle: Cycle = Cycle(workouts: [], load: .light)
    
    let unitOfMass = UserDefaults.unitOfMass
    
    init() {
        self.anyCancellable = $week
            .compactMap {
                Load(rawValue: $0)
            }.combineLatest(self.repository.workouts)
            .sink(receiveValue: { load, workouts in
                self.cycle = Cycle(workouts: workouts, load: load)
            })
    }
}
