import SwiftUI

struct WorkoutView: View {
    @Environment(\.colorScheme) var colorScheme
    var workout: Workout
    var load: Load

    private var weights: [Double] {
        return WeightCalculator().calculateWeights(
            oneRm: workout.exercise.weight,
            load: load.percentages,
            rounding: UserDefaults.smallIncrease
        )
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            HStack {
                Text("\(workout.exercise.name)")
                    .font(.system(size: 22))
                Spacer()
            }

            VStack(spacing: 0) {
                SetView(set: WorkoutSet(number: 1,
                                        reps: load.reps[0],
                                        weight: weights[0],
                                        unitOfMass: UserDefaults.unitOfMass))
                SetView(set: WorkoutSet(number: 2,
                                        reps: load.reps[1],
                                        weight: weights[1],
                                        unitOfMass: UserDefaults.unitOfMass))
                SetView(set: WorkoutSet(number: 3,
                                        reps: load.reps[2],
                                        weight: weights[2],
                                        unitOfMass: UserDefaults.unitOfMass))
            }

            if !workout.accessoryExercises.isEmpty {
                Text("Accessory exercises")
                    .foregroundColor(Color.secondary)

                VStack(spacing: 15) {
                    ForEach(workout.accessoryExercises, id: \.id) { exercise in
                        AccessoryExerciseView(accessoryExercise: exercise)
                    }
                }
            }
        }
        .padding()
        .background(colorScheme == .dark ? Color(UIColor.secondarySystemBackground) : Color(UIColor.systemBackground))
        .cornerRadius(5)
        .shadow(radius: 3)
    }
}

struct CycleView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            WorkoutView(
                workout: SampleData.workouts[0], load: .light
            ).colorScheme(.light)

            WorkoutView(
                workout: SampleData.workouts[0], load: .light
            ).colorScheme(.dark)
        }
    }
}
