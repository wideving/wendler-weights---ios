import SwiftUI

struct CyclesView: View {
    @ObservedObject var viewModel = CycleViewModel()

    var body: some View {
        NavigationView {
            VStack {
                if viewModel.cycle.workouts.isEmpty {
                    EmptyView(text: "No workouts have been added yet. Go to setup to begin adding workouts")
                } else {
                    ZStack(alignment: .bottom) {
                        ScrollView {
                            VStack {
                                VStack(spacing: 15) {
                                    ForEach(viewModel.cycle.workouts,
                                            id: \.id) { workout in
                                        WorkoutView(workout: workout,
                                                    load: self.viewModel.cycle.load)
                                    }
                                }
                                .padding()
                            }
                            .padding(.bottom, 90)
                        }
                        CyclePickerView(
                            pickedValue: $viewModel.week,
                            titles: self.viewModel.weekTitles
                        )
                    }
                    .edgesIgnoringSafeArea(.bottom)
                }
            }
            .navigationBarTitle("Wendler Weights", displayMode: .inline)
            .navigationBarItems(
                leading:
                NavigationLink(destination: PreferencesView()) {
                    Image(systemName: "gear")
                        .defaultFont()
                },
                trailing:
                NavigationLink(destination: SetupView()) {
                    Image(systemName: "square.and.pencil")
                        .defaultFont()
                }
            )
        }
    }
}

struct CyclesView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CyclesView()
                .colorScheme(.light)
        }
    }
}
