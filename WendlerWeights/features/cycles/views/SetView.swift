import SwiftUI

struct SetView: View {
    var set: WorkoutSet

    var body: some View {
        HStack {
            Image(systemName: "\(set.number).circle")
                .foregroundColor(Color.secondary)
                .font(.system(size: 20))
                .padding(.vertical, 10)
            Spacer()
            Text("\(String(format: "%.1f", set.weight))")
                .bold()
            Text(set.unitOfMass)
                .foregroundColor(Color.secondary)
            Text("x")
                .frame(width: 40, alignment: .trailing)
                .foregroundColor(Color.secondary)
            Text("\(set.reps)")
                .bold()
                .frame(width: 50, alignment: .trailing)
            Text("reps")
                .foregroundColor(Color.secondary)
        }
    }
}

struct SetView_Previews: PreviewProvider {
    static var previews: some View {
        SetView(set: WorkoutSet(number: 1,
                                reps: "5",
                                weight: 100,
                                unitOfMass: "kg"))
    }
}
