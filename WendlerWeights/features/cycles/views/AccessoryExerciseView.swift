import SwiftUI

struct AccessoryExerciseView: View {
    var accessoryExercise: AccessoryExercise

    var body: some View {
        HStack {
            Text(accessoryExercise.name)
            Spacer()
            Text("\(accessoryExercise.sets)")
                .bold()
            Text("sets")
                .foregroundColor(Color.secondary)
            Text("x")
                .foregroundColor(Color.secondary)
                .frame(width: 40, alignment: .trailing)
            Text("\(accessoryExercise.reps)")
                .bold()
                .frame(width: 50, alignment: .trailing)
            Text("reps")
                .foregroundColor(Color.secondary)
        }
    }
}

struct AccessoryExerciseView_Previews: PreviewProvider {
    static var previews: some View {
        AccessoryExerciseView(
            accessoryExercise: AccessoryExercise(
                name: "Front squat",
                sets: 5,
                reps: 15
            )
        )
    }
}
