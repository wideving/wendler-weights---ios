import SwiftUI

struct CyclePickerView: View {
    @Environment(\.colorScheme) private var colorScheme
    @Binding var pickedValue: Int
    var titles: [String]

    var body: some View {
        let bottomColor: Color = colorScheme == .dark ? .black : .white
        let topColor: Color = colorScheme == .dark ? .clear : Color.white.opacity(0.0)

        let gradientColors = Gradient(stops: [
            .init(color: topColor, location: 0),
            .init(color: bottomColor, location: 0.5)
        ])

        return VStack {
            ZStack {
                LinearGradient(gradient: gradientColors, startPoint: .top, endPoint: .bottom)
                    .frame(height: 150)
                Picker(selection: self.$pickedValue, label: Text("Picker")) {
                    ForEach(0..<self.titles.count) {
                        Text(self.titles[$0])
                    }
                }
                .padding()
                .pickerStyle(SegmentedPickerStyle())
            }
        }
    }
}

struct CyclePickerView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CyclePickerView(pickedValue: .constant(0), titles: ["Light", "Moderate", "Heavy", "Deload"])
                .colorScheme(.light)
            CyclePickerView(pickedValue: .constant(0), titles: ["Light", "Moderate", "Heavy", "Deload"])
                .colorScheme(.dark)
        }
    }
}
