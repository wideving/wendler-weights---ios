import SwiftUI

enum SetupAction {
    case increase
    case decrease
    case edit
}

struct SetupWorkoutView: View {
    @Environment(\.colorScheme) var colorScheme
    var workout: Workout

    let action: (SetupAction) -> Void

    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            HStack {
                Text(workout.exercise.name)
                    .font(.system(size: 22))
                Spacer()
                Text("\(String(format: "%.1f", workout.exercise.weight)) \(UserDefaults.unitOfMass.uppercased())")
                    .font(.system(size: 22))
            }
            HStack {
                Text("Cycle increment:")
                    .foregroundColor(Color.secondary)
                Text("\(String(format: "%.1f", workout.exercise.increment))")
                Text(UserDefaults.unitOfMass)
                    .foregroundColor(Color.secondary)
            }

            if !workout.accessoryExercises.isEmpty {
                Divider()
                VStack(alignment: .leading, spacing: 15) {
                    ForEach(workout.accessoryExercises, id: \.id) { accessoryExercise in
                        AccessoryExerciseView(accessoryExercise: accessoryExercise)
                    }
                }
            }

            HStack(alignment: .center, spacing: 30) {
                Button("Increase", action: {
                    self.action(.increase)
                    self.simpleSuccess()
                })
                    .buttonStyle(PrimaryButtonStyle())
                    .layoutPriority(2)

                Button("Decrease", action: {
                    self.action(.decrease)
                    self.simpleSuccess()
                })
                    .foregroundColor(Color.primary)
                    .layoutPriority(1)
                Spacer()
                Button(action: {
                    self.action(.edit)
                }, label: {
                    Image(systemName: "square.and.pencil")
                        .defaultFont()
                        .frame(width: 40, height: 40, alignment: .center)
                        .foregroundColor(Color.appSecondary)
                })
            }
        }
        .padding()
        .background(colorScheme == .dark ? Color(UIColor.secondarySystemBackground) : Color(UIColor.systemBackground))
        .cornerRadius(5)
        .shadow(radius: 3)
    }

    func simpleSuccess() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
}

struct SetupWorkoutView_Previews: PreviewProvider {
    static func previewAction(action: SetupAction) {}

    static var previews: some View {
        Group {
            SetupWorkoutView(workout: SampleData.workouts[0],
                             action: previewAction(action:))
                .colorScheme(.dark)
            SetupWorkoutView(workout: SampleData.workouts[0],
                             action: previewAction(action:))
                .colorScheme(.light)
        }
    }
}
