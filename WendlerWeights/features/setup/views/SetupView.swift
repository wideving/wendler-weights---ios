import SwiftUI

struct SetupView: View {
    private let viewModel = SetupViewModel()
    @State var showAddSheet: Bool = false
    @State var workout: Workout? = nil

    var body: some View {
        VStack {
            if viewModel.workouts.isEmpty {
                EmptyView(text: "Your added workouts will be displayed here")
            } else {
                List {
                    ForEach(viewModel.workouts, id: \.id) { workout in
                        SetupWorkoutView(workout: workout) { action in
                            switch action {
                            case .increase:
                                self.viewModel.increase(workout: workout)
                            case .decrease:
                                self.viewModel.decrease(workout: workout)
                            case .edit:
                                self.workout = workout
                                self.showAddSheet = true
                            }
                        }
                    }.onDelete(perform: viewModel.removeWorkout(at:))
                }
            }
        }
        .buttonStyle(PlainButtonStyle())
        .navigationBarTitle("Setup")
        .navigationBarItems(trailing:
            Button(action: {
                self.workout = nil
                self.showAddSheet = true
            }, label: {
                Image(systemName: "plus.circle")
                    .defaultFont()
            })
        )
        .sheet(isPresented: $showAddSheet) {
            AddView(workout: self.workout)
        }
        .onAppear {
            UITableView.appearance().separatorStyle = .none
        }.onDisappear {
            UITableView.appearance().separatorStyle = .singleLine
        }
    }
}

struct SetupView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SetupView()
        }
    }
}
