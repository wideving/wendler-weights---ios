import Combine
import Foundation

class SetupViewModel: ObservableObject {
    private let repository = WorkoutRepository()
    @Published private(set) var workouts: [Workout] = []
    
    private var cancelBag = Set<AnyCancellable>()
    
    init() {
        repository.workouts.sink { workouts in
            self.workouts = workouts
        }.store(in: &cancelBag)
    }
    
    func removeWorkout(at offsets: IndexSet) {
        repository.removeWorkout(at: offsets)
    }
    
    func addWorkout(workout: Workout) {
        repository.saveWorkout(workout: workout)
    }
    
    func increase(workout: Workout) {
        let editExercise = workout.exercise.copy(
            weight: workout.exercise.weight + workout.exercise.increment
        )
        
        repository.saveWorkout(workout: workout.copy(exercise: editExercise))
    }
    
    func decrease(workout: Workout) {
        let editExercise = workout.exercise.copy(
            weight: workout.exercise.weight - workout.exercise.increment
        )
        
        repository.saveWorkout(workout: workout.copy(exercise: editExercise))
    }
}
