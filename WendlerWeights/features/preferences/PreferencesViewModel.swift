import Combine
import Foundation

class PreferencesViewModel: ObservableObject {
    private var cancelBag = Set<AnyCancellable>()

    @Published var useLbs: Bool {
        didSet {
            UserDefaults.unitOfMass = useLbs ? "lbs" : "kg"
            UserDefaults.smallIncrease = useLbs ? 5.0 : 2.5
            UserDefaults.largeIncrease = useLbs ? 10.0 : 5.0
        }
    }

    init() {
        useLbs = UserDefaults.unitOfMass == "lbs"
    }
}
