import SwiftUI

struct PreferencesView: View {
    @ObservedObject private var viewModel = PreferencesViewModel()

    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            Toggle(isOn: self.$viewModel.useLbs) {
                Text("Use lbs instead of kg")
            }
            Spacer()
        }
        .padding()
    }
}

struct PreferencesView_Previews: PreviewProvider {
    static var previews: some View {
        PreferencesView()
    }
}
