import SwiftUI

struct AddAccessoryExerciseView: View {
    @EnvironmentObject var viewModel: AddViewModel
    @Binding var showAddAccessoryExerciseView: Bool

    var body: some View {
        VStack(alignment: .leading, spacing: 25) {
            Text("Add accessory exercise")
                .bold()
            HStack(spacing: 15) {
                TextField("Exercise name", text: $viewModel.accessoryName)
                    .keyboardType(.default)
                    .disableAutocorrection(true)
                TextField("Sets", text: $viewModel.accessorySets)
                    .keyboardType(.numberPad)
                    .frame(width: 75)
                TextField("Reps", text: $viewModel.accessoryReps)
                    .keyboardType(.numberPad)
                    .frame(width: 75)
            }
            HStack(spacing: 15) {
                Button(action: {
                    self.showAddAccessoryExerciseView = false
                }, label: {
                    Text("Cancel")
                }).buttonStyle(PrimaryButtonStyle(backgroundColor: .red))

                Button(action: {
                    self.viewModel.addAccessoryExercise()
                    self.showAddAccessoryExerciseView = false
                }, label: {
                    Text("Save")
                        .frame(maxWidth: .infinity)
                })
                    .buttonStyle(PrimaryButtonStyle(
                        backgroundColor: self.viewModel.isAccessoryValid ?
                            .appSecondary : .gray
                ))
                    .disabled(!self.viewModel.isAccessoryValid)
            }
        }
        .textFieldStyle(RoundedBorderTextFieldStyle())
        .accentColor(Color.secondary)
        .onDisappear {
            self.viewModel.accessoryName = ""
            self.viewModel.accessoryReps = ""
            self.viewModel.accessorySets = ""
            UIApplication
                .shared
                .sendAction(#selector(UIResponder.resignFirstResponder),
                            to: nil,
                            from: nil,
                            for: nil)
        }
    }
}

struct AddAccessoryExerciseView_Previews: PreviewProvider {
    static var previews: some View {
        AddAccessoryExerciseView(showAddAccessoryExerciseView: .constant(true))
    }
}
