import SwiftUI

struct AccessoryExercisesInfoView: View {
    @Binding var showAddAccessoryExerciseView: Bool
    var body: some View {
        HStack(spacing: 15) {
            VStack(alignment: .leading, spacing: 15) {
                Text("Accessory exercises")
                    .bold()
                Text("These exercises will be performed after the main compound movement")
            }
            Button(action: {
                self.showAddAccessoryExerciseView.toggle()
            }, label: {
                Image(systemName: "plus")
                    .defaultFont()
                    .foregroundColor(.white)
                    .padding()
            })
                .buttonStyle(FabStyle())
        }
    }
}

struct AccessoryExercisesInfoView_Previews: PreviewProvider {
    static var previews: some View {
        AccessoryExercisesInfoView(showAddAccessoryExerciseView: .constant(true))
    }
}
