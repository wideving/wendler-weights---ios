import Combine
import SwiftUI

struct AddView: View {
    @State var showAddAccessoryExerciseView: Bool = false
    @ObservedObject var viewModel: AddViewModel
    @Environment(\.presentationMode) var presentationMode

    init(workout: Workout? = nil) {
        viewModel = AddViewModel(workout: workout)
    }

    var body: some View {
        NavigationView {
            VStack(alignment: .leading, spacing: 15) {
                MainCompoundMovementView(name: $viewModel.name, oneRm: $viewModel.oneRm)
                IncrementView(increment: $viewModel.increment, titles: viewModel.incrementTitles)
                Divider()
                VStack {
                    if self.showAddAccessoryExerciseView {
                        AddAccessoryExerciseView(
                            showAddAccessoryExerciseView: $showAddAccessoryExerciseView
                        )
                        .environmentObject(viewModel)
                    } else {
                        AccessoryExercisesInfoView(
                            showAddAccessoryExerciseView: self.$showAddAccessoryExerciseView)
                    }
                }
                List {
                    ForEach(viewModel.accessoryExercises, id: \.id) {
                        AccessoryExerciseView(accessoryExercise: $0)
                            .listRowInsets(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
                    }
                    .onDelete(perform: viewModel.deleteAccessoryExercise(at:))
                }
            }
            .padding()
            .textFieldStyle(RoundedBorderTextFieldStyle())
            .navigationBarTitle(Text(self.viewModel.title), displayMode: .inline)
            .navigationBarItems(leading:
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }, label: {
                    Text("Cancel")
                }), trailing:
                Button(action: {
                    self.simpleSuccess()
                    self.viewModel.saveWorkout()
                    self.presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "checkmark")
                        .defaultFont()
                })
                    .disabled(viewModel.disableSave))
        }
    }

    func simpleSuccess() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
}

struct AddView_Previews: PreviewProvider {
    static var previews: some View {
        AddView()
    }
}
