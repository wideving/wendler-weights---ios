import SwiftUI

struct IncrementView: View {
    @Binding var increment: Int
    let titles: [String]

    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            Text("How much weight will be added at the end of each cycle")
                .fixedSize(horizontal: false, vertical: true)
                .foregroundColor(Color.secondary)
            Picker(selection: $increment, label: Text("Increment")) {
                ForEach(0..<titles.count) {
                    Text(self.titles[$0])
                }
            }
            .pickerStyle(SegmentedPickerStyle())
        }
    }
}

struct IncrementView_Previews: PreviewProvider {
    static var previews: some View {
        IncrementView(
            increment: .constant(0),
            titles: ["2.5 kg", "5.0 kg"]
        )
    }
}
