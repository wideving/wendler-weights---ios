import SwiftUI

struct MainCompoundMovementView: View {
    @Binding var name: String
    @Binding var oneRm: String

    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            Text("Main compound movement")
                .font(.system(size: 22))
            Text("Add your main compound movement for this workout. Weights will be calculated from your 1RM.")
                .fixedSize(horizontal: false, vertical: true)
                .foregroundColor(Color.secondary)
            HStack {
                TextField("Exercise name", text: $name)
                    .keyboardType(.default)
                    .disableAutocorrection(true)
                TextField("1RM", text: $oneRm)
                    .frame(width: 100, alignment: .leading)
                    .keyboardType(.decimalPad)
                    .padding(.leading)
            }
            .textFieldStyle(RoundedBorderTextFieldStyle())
        }
    }
}

struct MainCompoundMovementView_Previews: PreviewProvider {
    static var previews: some View {
        MainCompoundMovementView(
            name: .constant("Squat"),
            oneRm: .constant("100.0")
        )
    }
}
