import Combine
import Foundation

class AddViewModel: ObservableObject {
    private var repository: WorkoutRepository = WorkoutRepository()
    
    let incrementTitles: [String]
    var increment: Int = 0
    
    @Published var name: String = ""
    @Published var oneRm: String = ""
    @Published var accessoryExercises: [AccessoryExercise] = []
    
    @Published var accessoryName: String = ""
    @Published var accessorySets: String = ""
    @Published var accessoryReps: String = ""
    
    var title: String = "Add workout"
    
    var disableSave: Bool = true
    
    @Published var isAccessoryValid: Bool = false
    
    private var cancelBag = Set<AnyCancellable>()
    private var workout: Workout?
    
    init(workout: Workout? = nil) {
        self.workout = workout
        self.incrementTitles =  [
            "\(UserDefaults.smallIncrease) \(UserDefaults.unitOfMass)",
            "\(UserDefaults.largeIncrease) \(UserDefaults.unitOfMass)"
        ]
        if let editWorkout = self.workout {
            title = "Edit workout"
            name = editWorkout.exercise.name
            oneRm = String(editWorkout.exercise.weight)
            accessoryExercises = editWorkout.accessoryExercises
            increment = editWorkout.exercise.increment == UserDefaults.smallIncrease ? 0 : 1
        }
        
        $name
            .combineLatest($oneRm) { name, oneRm in
                name.count > 2 && (Double(oneRm.replacingOccurrences(of: ",", with: ".")) != nil)
            }
            .sink { valid in
                self.disableSave = !valid
            }.store(in: &cancelBag)
        
        $accessoryName.combineLatest(
            $accessorySets,
            $accessoryReps
        ) { aName, aSets, aReps in
            aName.count > 2 && (Int(aSets) != nil) && (Int(aReps) != nil)
        }.sink(receiveValue: { valid in
            self.isAccessoryValid = valid
        }).store(in: &cancelBag)
    }
    
    func deleteAccessoryExercise(at offsets: IndexSet) {
        accessoryExercises.remove(atOffsets: offsets)
    }
    
    func addAccessoryExercise() {
        accessoryExercises.append(
            AccessoryExercise(
                name: accessoryName,
                sets: Int(accessorySets) ?? 0,
                reps: Int(accessoryReps) ?? 0
            )
        )
    }
    
    func saveWorkout() {
        // Editing workout
        if let workout = workout {
            let editWorkout = workout.copy(
                exercise: Exercise(
                    name: name,
                    weight: Double(oneRm.replacingOccurrences(of: ",", with: ".")) ?? 0,
                    increment: increment == 0 ? UserDefaults.smallIncrease : UserDefaults.largeIncrease
                ), accessoryExercises: accessoryExercises
            )
            repository.saveWorkout(workout: editWorkout)
        } else {
            let workout = Workout(
                exercise: Exercise(
                    name: name,
                    weight: Double(oneRm.replacingOccurrences(of: ",", with: ".")) ?? 0,
                    increment: increment == 0 ? UserDefaults.smallIncrease : UserDefaults.largeIncrease
                ), accessoryExercises: accessoryExercises
            )
            repository.saveWorkout(workout: workout)
        }
    }
}
