import SwiftUI

struct EmptyView: View {
    let text: String
    var body: some View {
        VStack {
            Image("EmptyList")
                .resizable()
                .frame(width: 300, height: 300, alignment: .center)
                .foregroundColor(Color.secondary)
            Text(text)
                .multilineTextAlignment(.center)
                .foregroundColor(Color.secondary)
        }.padding()
    }
}

struct EmptyView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView(text: "No workouts have been added yet. Go to setup to begin adding workouts")
        
    }
}
