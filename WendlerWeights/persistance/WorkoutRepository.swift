import Combine
import Foundation

class WorkoutRepository: ObservableObject {
    private var _workouts: CurrentValueSubject<[Workout], Never>
    var workouts: AnyPublisher<[Workout], Never>
    
    private var cancelBag = Set<AnyCancellable>()
    
    init() {
        _workouts = CurrentValueSubject<[Workout], Never>(UserDefaults.workouts)
        workouts = _workouts.eraseToAnyPublisher()
        
        NotificationCenter.default.publisher(for: UserDefaults.didChangeNotification)
            .sink { _ in
                self._workouts.send(UserDefaults.workouts)
            }.store(in: &cancelBag)
    }
    
    func saveWorkout(workout: Workout) {
        var savedWorkouts = UserDefaults.workouts
        
        if let oldIndex = savedWorkouts.firstIndex(where: { $0.id == workout.id }) {
            savedWorkouts.remove(at: oldIndex)
            savedWorkouts.insert(workout, at: oldIndex)
        } else {
            savedWorkouts.append(workout)
        }
        
        UserDefaults.workouts = savedWorkouts
    }
    
    func removeWorkout(at offsets: IndexSet) {
        var savedWorkouts = UserDefaults.workouts
        savedWorkouts.remove(atOffsets: offsets)
        UserDefaults.workouts = savedWorkouts
    }
}
