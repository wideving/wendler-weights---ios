import Foundation

extension UserDefaults {
    static var workouts: [Workout] {
        get {
            guard let data = UserDefaults.standard.data(forKey: "workouts") else {
                return []
            }
            
            return (try? JSONDecoder().decode([Workout].self, from: data)) ?? []
        }
        
        set {
            let data = try? JSONEncoder().encode(newValue)
            UserDefaults.standard.set(data, forKey: "workouts")
        }
    }
    
    static var unitOfMass: String {
        get {
            UserDefaults.standard.string(forKey: Keys.unitOfMass) ?? "kg"
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.unitOfMass)
        }
    }
    
    static var smallIncrease: Double {
        get {
            let small = UserDefaults.standard.double(forKey: Keys.smallIncrease)
            return small == 0 ? 2.5 : small
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.smallIncrease)
        }
    }
    
    static var largeIncrease: Double {
        get {
            let large = UserDefaults.standard.double(forKey: Keys.largeIncrease)
            return large == 0 ? 5.0 : large
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.largeIncrease)
        }
    }
}
