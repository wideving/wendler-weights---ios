import Foundation

class MigrationHandler {
    let fileName = "Exercises"

    private let fileHandler = FileHandler<[OldExercise]>()

    func migrate() {
        if let oldExercises = fileHandler.read(fileName: fileName), !oldExercises.isEmpty {
            let workouts: [Workout] = oldExercises.map {
                let exercise = ExerciseConverter.convert(oldExercise: $0)
                return Workout(exercise: exercise, accessoryExercises: [])
            }

            UserDefaults.workouts = workouts
            _ = fileHandler.write(fileName: fileName, contents: [])
        }

        UserDefaults.standard.set(true, forKey: Keys.migrationDone)
    }
}
