import Foundation

struct FileHandler<T: Codable> {
    func read(fileName: String) -> T? {
        let decoder = PropertyListDecoder()
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            .appendingPathComponent("\(fileName).plist")
        
        do {
            let data = try Data(contentsOf: path)
            return try decoder.decode(T.self, from: data)
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func write(fileName: String, contents: T) -> Bool {
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            .appendingPathComponent("\(fileName).plist")
        
        do {
            let data = try encoder.encode(contents)
            try data.write(to: path)
            return true
        } catch {
            return false
        }
    }
    
    private func getDocumentPath(fileName: String) -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            .appendingPathComponent(fileName, isDirectory: false)
    }
}
