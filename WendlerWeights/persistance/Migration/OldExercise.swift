import Foundation

struct OldExercise: Codable {
    let name: String
    let maxWeight: Double
    let weightIncrease: Double
}
