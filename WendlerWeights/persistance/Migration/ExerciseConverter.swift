import Foundation

class ExerciseConverter {
    static func convert(oldExercise: OldExercise) -> Exercise {
        return Exercise(
            name: oldExercise.name,
            weight: oldExercise.maxWeight,
            increment: oldExercise.weightIncrease
        )
    }
}
