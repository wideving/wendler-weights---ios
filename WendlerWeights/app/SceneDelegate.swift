import UIKit
import SwiftUI

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        UISegmentedControl.defaultStyling()
        
        let rootView = CyclesView()

        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = UIHostingController(rootView: rootView)
            self.window = window
            self.window?.tintColor = UIColor(named: "Secondary")
            window.makeKeyAndVisible()
        }
    }

}


struct SceneDelegate_Previews: PreviewProvider {
    static var previews: some View {
        CyclesView()
    }
}
