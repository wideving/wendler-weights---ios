import IQKeyboardManagerSwift
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarTintColor = UIColor(named: "Secondary")

        if !UserDefaults.standard.bool(forKey: Keys.migrationDone) {
            let migrationHandler = MigrationHandler()
            migrationHandler.migrate()
        }

        return true
    }
}
