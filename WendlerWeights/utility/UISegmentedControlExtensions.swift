import UIKit

extension UISegmentedControl {
    static func defaultStyling() {
        UISegmentedControl.appearance().selectedSegmentTintColor = UIColor(named: "Secondary")
        UISegmentedControl.appearance().backgroundColor = UIColor(named: "Primary")
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .normal)
    }
}
