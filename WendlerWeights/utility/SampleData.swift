import Foundation

struct SampleData {
    static var workouts: [Workout] {
        [
            Workout(
                exercise: Exercise(
                    name: "Squat",
                    weight: 100.0,
                    increment: 2.5
                ),
                accessoryExercises: [
                    AccessoryExercise(name: "Leg press", sets: 5, reps: 15),
                    AccessoryExercise(name: "Leg curl", sets: 5, reps: 12)
                ]
            ),
            Workout(
                exercise: Exercise(
                    name: "Deadlift",
                    weight: 100.0,
                    increment: 2.5
                ),
                accessoryExercises: [
                    AccessoryExercise(name: "Good mornings", sets: 5, reps: 15),
                    AccessoryExercise(name: "Hanging leg raises", sets: 5, reps: 12)
                ]
            ),
            Workout(
                exercise: Exercise(
                    name: "Bench press",
                    weight: 100.0,
                    increment: 2.5
                ),
                accessoryExercises: [
                    AccessoryExercise(name: "Db press", sets: 5, reps: 15),
                    AccessoryExercise(name: "Db row", sets: 5, reps: 12)
                ]
            ),
            
            
        ]
        
    }
}
