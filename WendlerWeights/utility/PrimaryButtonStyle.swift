import SwiftUI

struct PrimaryButtonStyle: ButtonStyle {
    var backgroundColor: Color = .appSecondary
    
    
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(.horizontal)
            .padding(.vertical, 8)
            .foregroundColor(.white)
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .foregroundColor(backgroundColor)
                    .shadow(radius: configuration.isPressed ? 2 : 3)
            )
            .scaleEffect(configuration.isPressed ? 0.95 : 1.0,
                         anchor: .center)
            .animation(.spring())
    }
}
