import SwiftUI

extension Color {
    static let appPrimary = Color(UIColor(named: "Primary")!)
    static let appSecondary = Color(UIColor(named: "Secondary")!)
}
