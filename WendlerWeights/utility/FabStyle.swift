import SwiftUI

struct FabStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .background(Circle()
                .frame(width: 50, height: 50)
                .foregroundColor(Color.appSecondary)
                .shadow(radius: configuration.isPressed ? 2 : 3))
            .scaleEffect(configuration.isPressed ? 0.95 : 1.0,
                         anchor: .center)
            .animation(.spring())
    }
}
