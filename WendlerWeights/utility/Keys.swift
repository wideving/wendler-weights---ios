import Foundation

struct Keys {
    static let unitOfMass = "unitOfMass"
    static let smallIncrease = "smallIncrease"
    static let largeIncrease = "largeIncrease"
    static let migrationDone = "migrationDone"
}
