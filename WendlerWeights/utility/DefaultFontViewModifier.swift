import SwiftUI

struct DefaultFontModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.system(size: 20,
                          weight: .regular,
                          design: .rounded))
    }
}

extension View {
    func defaultFont() -> some View {
        self.modifier(DefaultFontModifier())
    }
}
