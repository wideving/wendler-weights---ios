import Foundation

struct WeightCalculator {
    func calculateWeights(oneRm: Double,
                          load: [Double],
                          rounding: Double) -> [Double] {
        load.map { percentage in
            floor((oneRm * percentage) / rounding) * rounding
        }
    }
}
