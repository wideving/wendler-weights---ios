@testable import WendlerWeights
import XCTest

class WeightCalculatorTests: XCTestCase {
    var weightCalculator: WeightCalculator!

    override func setUpWithError() throws {
        weightCalculator = WeightCalculator()
    }

    override func tearDownWithError() throws {
        weightCalculator = nil
    }

    func testDeloadWeights() throws {
        let values = weightCalculator.calculateWeights(
            oneRm: 100.0,
            load: Load.deload.percentages,
            rounding: 2.5
        )

        let expectedValues = [40.0, 50.0, 60.0]

        XCTAssertEqual(values, expectedValues)
    }
    
    func testLightWeights() throws {
        let values = weightCalculator.calculateWeights(
            oneRm: 100.0,
            load: Load.light.percentages,
            rounding: 2.5
        )

        let expectedValues = [65.0, 75.0, 85.0]

        XCTAssertEqual(values, expectedValues)
    }
    
    func testMediumWeights() throws {
        let values = weightCalculator.calculateWeights(
            oneRm: 100.0,
            load: Load.medium.percentages,
            rounding: 2.5
        )

        let expectedValues = [70.0, 80.0, 90.0]

        XCTAssertEqual(values, expectedValues)
    }
    
    func testHeavyWeights() throws {
        let values = weightCalculator.calculateWeights(
            oneRm: 100.0,
            load: Load.heavy.percentages,
            rounding: 2.5
        )

        let expectedValues = [85.0, 90.0, 95.0]

        XCTAssertEqual(values, expectedValues)
    }
    
    func testTwoAndAHalfRounding() throws {
        let values = weightCalculator.calculateWeights(
            oneRm: 102.5,
            load: Load.heavy.percentages,
            rounding: 2.5
        )
        
        let expectedValues = [85.0, 90.0, 95.0]
        
        XCTAssertEqual(values, expectedValues)
        
    }
    
    func testFiveRounding() throws {
        let values = weightCalculator.calculateWeights(
            oneRm: 235.0,
            load: Load.heavy.percentages,
            rounding: 5.0
        )
        
        let expectedValues = [195.0, 210.0, 220.0]
        
        XCTAssertEqual(values, expectedValues)
        
    }
}
